import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddentreprisesComponent } from './addentreprises.component';

describe('AddentreprisesComponent', () => {
  let component: AddentreprisesComponent;
  let fixture: ComponentFixture<AddentreprisesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddentreprisesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddentreprisesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
