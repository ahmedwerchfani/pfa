import { Component, OnInit, NgModule } from '@angular/core';
import { Entreprise } from '../classes/entreprise';
import { EntrepriseService } from '../services/entreprise.service';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { AppComponent } from '../app.component';


@Component({
  selector: 'app-addentreprises',
  templateUrl: './addentreprises.component.html',
  styleUrls: ['./addentreprises.component.css']
})
@NgModule({
  imports: [
    BrowserModule,
    FormsModule],
  declarations: [
    AppComponent
  ],
  bootstrap: [AppComponent]
})
export class AddentreprisesComponent implements OnInit {
  entr: Entreprise;
  submitted = false;
  constructor(private entrservice: EntrepriseService) { }

  ngOnInit() {
    this.submitted = false;
    this.entr = new Entreprise();
  }
  onSubmit() {
    this.submitted = true;
    this.save();
  }
  private save(): void {
    this.entrservice.create(this.entr);

  }
  goBack(): void {
    window.location.replace('accueil');
  }


}