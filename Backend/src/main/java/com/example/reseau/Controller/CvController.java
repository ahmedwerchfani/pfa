/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.reseau.Controller;

import Exception.ResourceNotFoundException;
import com.example.reseau.Entities.Cv;
import com.example.reseau.Repository.CvRepository;
import javax.transaction.Transactional;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;


/**
 *
 * @author orange
 */
@RestController //pour application rest resultat json et xml
@Transactional //accepte les transaction des autre application
@RequestMapping("/api")
@CrossOrigin(origins = "localhost:4200", maxAge = 3600)//le port de angulair et maxage pour securite
public class CvController {

    @Autowired //
    CvRepository cvRepository;

    // Get All Cvs
@GetMapping("/cv")
public List<Cv> getCvs() {
    return (List<Cv>) cvRepository.findAll();
}
// Create a new Cv
@PostMapping("/cvs")
public Cv createCv(@Valid @RequestBody Cv cv) {
    return cvRepository.save(cv);
}

// Get a Single Cv
@GetMapping("/cvs/{id}")
public Cv getCvById(@PathVariable(value = "id") Long cvId) {
    return cvRepository.findById(cvId)
            .orElseThrow(() -> new ResourceNotFoundException("Cv", "id", cvId));
}
    // Update a Cv
@PutMapping("/cvs/{id}")
public Cv updateCv(@PathVariable(value = "id") Long cvId,
                                        @Valid @RequestBody Cv cvDetails) {

    Cv cv = cvRepository.findById(cvId)
            .orElseThrow(() -> new ResourceNotFoundException("Cv", "id", cvId));

    cv.setTitre(cvDetails.getTitre());
    cv.setDomaine(cvDetails.getDomaine());
    cv.setNiveau(cvDetails.getNiveau());
    cv.setDiplome(cvDetails.getDiplome());
    cv.setCompetence(cvDetails.getCompetence());

    
    Cv updatedCv = cvRepository.save(cv);
    return updatedCv;
}


// Delete a Cv
@DeleteMapping("/cvs/{id}")
public ResponseEntity<?> deleteCv(@PathVariable(value = "id") Long cvId) {
    Cv cv = cvRepository.findById(cvId)
            .orElseThrow(() -> new ResourceNotFoundException("Cv", "id", cvId));

    cvRepository.delete(cv);

    return ResponseEntity.ok().build();
}
}
