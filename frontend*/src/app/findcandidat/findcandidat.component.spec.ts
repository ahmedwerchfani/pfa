import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FindcandidatComponent } from './findcandidat.component';

describe('FindcandidatComponent', () => {
  let component: FindcandidatComponent;
  let fixture: ComponentFixture<FindcandidatComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FindcandidatComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FindcandidatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
