import { TestBed } from '@angular/core/testing';

import { PostulerService } from './postuler.service';

describe('PostulerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PostulerService = TestBed.get(PostulerService);
    expect(service).toBeTruthy();
  });
});
