
import { Component, OnInit, Input } from '@angular/core';
import { Offre } from '../classes/offre';
import { OffreService } from '../services/offre.service';

@Component({
  selector: 'app-accueil',
  templateUrl: './accueil.component.html',
  styleUrls: ['./accueil.component.css']
})
export class AccueilComponent implements OnInit {
 
  offre: Offre;
  offres: Offre[];

  loading = true;
    constructor(private offreservice: OffreService) {
     }

  ngOnInit() {
    this.getUsers();
    


  }
  getUsers(){
    this.offreservice.getOffres().then(offres => this.offres = offres);
    this.loading = false;
   }
   onSelect(off: Offre): void {
     this.offre = off;
    console.log(off.id);
  }
  
  aff(): void {
   
    localStorage.setItem('idoff',String(this.offre.id));
    localStorage.setItem('identreprise',String(this.offre.identreprise));
    window.location.replace('jobdetail');
  
  }

  goBack(): void {
    window.location.replace('accueil');}

    onSearchChange(value: string) {
      this.loading = true;
      if (value === '') {
        this.getUsers();
        return;
      }
      this.offres= this.search(this.offres, value);
    }
  
    search(offres: Offre[], searchText: string): Offre[] {
      if (!offres) {
        return [];
      }
      if (!searchText) {
        return offres;
      }
      searchText = searchText.toLowerCase();
      return offres.filter(it => {
        return it.profil.toLowerCase().includes(searchText) || it.description.toLowerCase().includes(searchText);
      });
    }


  
}
