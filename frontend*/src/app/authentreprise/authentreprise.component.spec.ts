import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuthentrepriseComponent } from './authentreprise.component';

describe('AuthentrepriseComponent', () => {
  let component: AuthentrepriseComponent;
  let fixture: ComponentFixture<AuthentrepriseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuthentrepriseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuthentrepriseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
