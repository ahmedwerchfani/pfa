import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddcvsComponent } from './addcvs.component';

describe('AddcvsComponent', () => {
  let component: AddcvsComponent;
  let fixture: ComponentFixture<AddcvsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddcvsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddcvsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
