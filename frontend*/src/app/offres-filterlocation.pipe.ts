import { Pipe, PipeTransform } from '@angular/core';
import { Cv } from './classes/cv';
@Pipe({
  name: 'offresFilterlocation'
})
export class OffresFilterlocationPipe implements PipeTransform {

  transform(values) {
    if (values) {
      return values.reverse();
    }
  }
}
