import { Component, OnInit } from '@angular/core';
import { Entreprise } from '../classes/entreprise';
import { EntrepriseService } from '../services/entreprise.service';
@Component({
  selector: 'app-tablentreprise',
  templateUrl: './tablentreprise.component.html',
  styleUrls: ['./tablentreprise.component.css']
})
export class TablentrepriseComponent implements OnInit {
  
  entreprises:Entreprise[];
  entreprise:Entreprise;
    constructor(private entrepriseservice: EntrepriseService) {
  

     }

  ngOnInit() {
    if (localStorage.length === 0) {
          window.location.replace('auth');
    }
    
    this.getEntreprises();
  }
 
   getEntreprises(){
    this.entrepriseservice.getEntreprises().then(entreprises => this.entreprises = entreprises);
   }
   onSelect(en: Entreprise): void {
    this.entreprise = en;
    console.log(en.id);
  }
  delete(): void {

    this.entrepriseservice.delete(this.entreprise.id).then(() => this.goBack());
  }
  goBack(): void {
    window.location.replace('accueil');
  }
}
