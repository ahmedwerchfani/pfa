import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
 
  submitted = false;
  sub=false;
  type=localStorage.getItem('type')
  constructor() { }
     
  ngOnInit() {
  
   if (localStorage.length === 0) {
     this.submitted=false;    }
  else if(this.type=="entrep"){
    this.sub=true;
  }
  else if(this.type=="uss"){
    this.submitted=true;
  }
}

 }


