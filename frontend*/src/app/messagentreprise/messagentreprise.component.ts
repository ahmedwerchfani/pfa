import { Component, OnInit } from '@angular/core';
import { Postuler } from '../classes/postuler';
import { User } from '../classes/users';
import { UserService } from '../services/user.service';
import { PostulerService } from '../services/postuler.service';

@Component({
  selector: 'app-messagentreprise',
  templateUrl: './messagentreprise.component.html',
  styleUrls: ['./messagentreprise.component.css']
})
export class MessagentrepriseComponent implements OnInit {
  msgs:Postuler[];
  msg:Postuler;
  users:User[];
  user:User;
  is:string;
  a:number;
  recpt=(localStorage.getItem('id'));
  n=(localStorage.getItem('nom'));
    constructor(private msgservice:PostulerService, private userservice:UserService) {
  

     }

  ngOnInit() {
    
    
    this.getMsgs(this.recpt);
    this.is=this.msg.emetteur
    this.a=parseInt(this.is);
    this.getUsers(this.a);
    console.log(this.a);
     
  }
 
   getMsgs(i:string){
    this.msgservice.getmess(i).then(msgs => this.msgs = msgs);
   }
   getUsers(i:number) {
    this.userservice.getUser(i).then(users => this.user = users);
    
   }
   onSelect(ms: Postuler): void {
    this.msg = ms;
    console.log(ms.id);
  }
  delete(index): void { 
  this.msgs.splice(index,1);
  this.msgservice.delete(this.msg.id);
  
  }
  goBack(): void {
    window.location.replace('accueil');


    
  }
  deco()
{ 
  localStorage.clear(); 
  window.location.replace('accueil');
} 
}