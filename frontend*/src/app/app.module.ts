import { AccueilComponent } from './accueil/accueil.component';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UpComponent } from './up/up.component';
import { FileSelectDirective, FileDropDirective } from 'ng2-file-upload';
import { AuthComponent } from './auth/auth.component';

import { AddusersComponent } from './addusers/addusers.component';
import { MessagesComponent } from './messages/messages.component';
import { SendmessageComponent } from './sendmessage/sendmessage.component';
import { DocumentsComponent } from './documents/documents.component';
import { CreatedocumentsComponent } from './createdocuments/createdocuments.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import { OffreComponent } from './offre/offre.component';
import { OffresFilterdomainePipe } from './offres-filterdomaine.pipe';
import { OffresFilterlocationPipe } from './offres-filterlocation.pipe';
import { JobDetailsComponent } from './job-details/job-details.component';
import { ProfiladminComponent } from './profiladmin/profiladmin.component';
import { TablentrepriseComponent } from './tablentreprise/tablentreprise.component';
import { TableoffreComponent } from './tableoffre/tableoffre.component';

import { UserDetailsComponent } from './user-details/user-details.component';
import { RechercheComponent } from './recherche/recherche.component';
import { FindcandidatComponent } from './findcandidat/findcandidat.component';
import { AddentreprisesComponent } from './addentreprises/addentreprises.component';
import { AddcvsComponent } from './addcvs/addcvs.component';
import { AddparcoursComponent } from './addparcours/addparcours.component';
import { AddoffreComponent } from './addoffre/addoffre.component';
import { AffcvComponent } from './affcv/affcv.component';
import { JobdetailComponent } from './jobdetail/jobdetail.component';
import { AuthentrepriseComponent } from './authentreprise/authentreprise.component';
import { ProfilentrepComponent } from './profilentrep/profilentrep.component';
import { MessagentrepriseComponent } from './messagentreprise/messagentreprise.component';
import { CvutilisateurComponent } from './cvutilisateur/cvutilisateur.component';






@NgModule({
  declarations: [
    AppComponent,
    UpComponent,
    AccueilComponent,
  	AuthComponent,
 
	AddusersComponent,
	MessagesComponent,
	SendmessageComponent,
	DocumentsComponent,
	CreatedocumentsComponent,
	HeaderComponent,
  FooterComponent,
  FileSelectDirective,
  OffreComponent,
  OffresFilterdomainePipe,
  OffresFilterlocationPipe,
  JobDetailsComponent,
  ProfiladminComponent,
  TablentrepriseComponent,
  TableoffreComponent,
  
  UserDetailsComponent,
  RechercheComponent,
  FindcandidatComponent,
  AddentreprisesComponent,
  AddcvsComponent,
  AddparcoursComponent,
  AddoffreComponent,
  AffcvComponent,
  JobdetailComponent,
  AuthentrepriseComponent,
  ProfilentrepComponent,
  MessagentrepriseComponent,
  CvutilisateurComponent,
  
  

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
