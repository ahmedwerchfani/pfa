/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.reseau.Controller;

import Exception.ResourceNotFoundException;
import com.example.reseau.Entities.Scolarite;
import com.example.reseau.Repository.ScolariteRepository;
import javax.transaction.Transactional;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;


/**
 *
 * @author orange
 */
@RestController //pour application rest resultat json et xml
@Transactional //accepte les transaction des autre application
@RequestMapping("/api")
@CrossOrigin(origins = "localhost:4200", maxAge = 3600)//le port de angulair et maxage pour securite
public class ScolariteController {

    @Autowired //
    ScolariteRepository scolariteRepository;

    // Get All Scolarites
@GetMapping("/scolarite")
public List<Scolarite> getScolarites() {
    return (List<Scolarite>) scolariteRepository.findAll();
}
// Create a new Scolarite
@PostMapping("/scolarites")
public Scolarite createScolarite(@Valid @RequestBody Scolarite scolarite) {
    return scolariteRepository.save(scolarite);
}

// Get a Single Scolarite
@GetMapping("/scolarites/{id}")
public Scolarite getScolariteById(@PathVariable(value = "id") Long scolariteId) {
    return scolariteRepository.findById(scolariteId)
            .orElseThrow(() -> new ResourceNotFoundException("Scolarite", "id", scolariteId));
}
    // Update a Scolarite

@GetMapping("/Scolaritess/{iduser}")
public List<Scolarite> getScolariteByIduser(@PathVariable Long iduser) {
    return (List<Scolarite>) scolariteRepository.findByIduser(iduser);

}
// Delete a Scolarite
@DeleteMapping("/scolarites/{id}")
public ResponseEntity<?> deleteScolarite(@PathVariable(value = "id") Long scolariteId) {
    Scolarite scolarite = scolariteRepository.findById(scolariteId)
            .orElseThrow(() -> new ResourceNotFoundException("Scolarite", "id", scolariteId));

    scolariteRepository.delete(scolarite);

    return ResponseEntity.ok().build();
}


@PutMapping("/scolarite/{id}")
public Scolarite updateScolarite(@PathVariable(value = "id") Long scolariteId,
                                        @Valid @RequestBody Scolarite scolariteDetails) {

    Scolarite scolarite = scolariteRepository.findById(scolariteId)
            .orElseThrow(() -> new ResourceNotFoundException("Scolarite", "id", scolariteId));

    scolarite.setLieu(scolariteDetails.getLieu());
    scolarite.setDebut(scolariteDetails.getDebut());
    

    scolarite.setPeriode(scolariteDetails.getPeriode());
    scolarite.setEtablissement(scolariteDetails.getEtablissement());
    scolarite.setDiplome(scolariteDetails.getDiplome());
    scolarite.setDescription(scolariteDetails.getDescription());

    Scolarite updatedScolarite = scolariteRepository.save(scolarite);
    return updatedScolarite;
}

}