import { Injectable } from '@angular/core';
import { Headers,Http } from '@angular/http';
import { Cv } from '../classes/cv';

@Injectable({
  providedIn: 'root'
})
export class CvService {
  private headers = new Headers({'Content-Type': 'application/json'});


  constructor(private http: Http) { }
   getCvs(): Promise<Cv[]> {
    return this.http.get('http://localhost:8080/api/cv')
      .toPromise()
      .then(response => response.json() as Cv[])
      .catch();
  }
  getCv(iduser:number): Promise<Cv> {
    return this.http.get(`http://localhost:8080/api/cvs/${iduser}`)
      .toPromise()
      .then(response => response.json() as Cv)
      .catch();
  }
  create(cv: Cv): Promise<Cv> {
    return this.http
      .post('http://localhost:8080/api/cvs', JSON.stringify(cv), {headers : this.headers})
      .toPromise()
      .then(res => res.json() as Cv)
      .catch(this.handleError);
  }
  createcv(id:number,cv:Cv): Promise<Cv> {
    return this.http
      .put(`http://localhost:8080/api/cvs/${id}`,JSON.stringify(cv), {headers : this.headers})
      .toPromise()
      .then(res => res.json() as Cv)
      .catch(this.handleError);
  }
  delete(id: number): Promise<void> {
    const url = `http://localhost:8080/api/cvs/${id}`;
    return this.http.delete(url, {headers: this.headers})
      .toPromise()
      .then(() => null)
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('Error', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}