import { Component, OnInit } from '@angular/core';
import { User } from '../classes/users';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-recherche',
  templateUrl: './recherche.component.html',
  styleUrls: ['./recherche.component.css']
})
export class RechercheComponent implements OnInit {
  users: User[];
  
  user: User;
  
  a=(localStorage.getItem('id'));
  
  id=parseInt(this.a);
  submitted = false;
  Success=false;
  constructor( private userservice:UserService) 
{
    
   }
   

  ngOnInit() {
    
  this.getUsers(this.id);
  this.submitted = false;
  //localStorage.setItem('nom',this.user.nom);

  }
  
  
getUsers(i:number) {
  this.userservice.getUser(i).then(users => this.user = users);
  
 }
 

 onSelect(us: User): void {
  this.user = us;
  console.log(us.id);
}

goBack(): void {
  window.location.replace('accueil');
}
deco()
{ 
  localStorage.clear(); 
  window.location.replace('accueil'); 
} 
onSubmit() {
  this.submitted = true;
  this.save();
}
private save(): void {
 
  this.userservice.createuser(this.user.id,this.user);
  this.Success=true;
}
}

 

    
   


