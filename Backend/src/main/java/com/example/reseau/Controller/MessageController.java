/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.reseau.Controller;

import Exception.ResourceNotFoundException;
import com.example.reseau.Entities.Message;
import com.example.reseau.Repository.MessageRepository;
import javax.transaction.Transactional;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;


/**
 *
 * @author orange
 */
@RestController //pour application rest resultat json et xml
@Transactional //accepte les transaction des autre application
@RequestMapping("/api")
@CrossOrigin(origins = "localhost:4200", maxAge = 3600)//le port de angulair et maxage pour securite
public class MessageController {

    @Autowired //
    MessageRepository messageRepository;

    // Get All Messages
@GetMapping("/message")
public List<Message> getMessages() {
    return (List<Message>) messageRepository.findAll();
}
// Create a new Message
@PostMapping("/messages")
public Message createMessage(@Valid @RequestBody Message message) {
    return messageRepository.save(message);
}

// Get a Single Message
@GetMapping("/messages/{id}")
public Message getMessageById(@PathVariable(value = "id") Long messageId) {
    return messageRepository.findById(messageId)
            .orElseThrow(() -> new ResourceNotFoundException("Message", "id", messageId));
}
    // Update a Message
@GetMapping("/messag/{recepteur}")
public List<Message> getMessageByRecepteur(@PathVariable String recepteur) {
    return messageRepository.findByRecepteur(recepteur);

}

// Delete a Message
@DeleteMapping("/messages/{id}")
public ResponseEntity<?> deleteMessage(@PathVariable(value = "id") Long messageId) {
    Message message = messageRepository.findById(messageId)
            .orElseThrow(() -> new ResourceNotFoundException("Message", "id", messageId));

    messageRepository.delete(message);

    return ResponseEntity.ok().build();
}
}