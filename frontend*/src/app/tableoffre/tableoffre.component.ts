import { Component, OnInit } from '@angular/core';
import { Offre } from '../classes/offre';
import { OffreService } from '../services/offre.service';

@Component({
  selector: 'app-tableoffre',
  templateUrl: './tableoffre.component.html',
  styleUrls: ['./tableoffre.component.css']
})
export class TableoffreComponent implements OnInit {

  offres:Offre[];
  offre:Offre;
    constructor(private offreservice: OffreService) {
  

     }

  ngOnInit() {
    if (localStorage.length === 0) {
          window.location.replace('auth');
    }
    
    this.getOffres();
  }
 
   getOffres(){
    this.offreservice.getOffres().then(offres=> this.offres = offres);
   }
   onSelect(off: Offre): void {
    this.offre= off;
    console.log(off.id);
  }
  delete(): void {

    this.offreservice.delete(this.offre.id).then(() => this.goBack());
  }
  goBack(): void {
    window.location.replace('accueil');
  }
}