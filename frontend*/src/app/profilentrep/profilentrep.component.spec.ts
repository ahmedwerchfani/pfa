import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfilentrepComponent } from './profilentrep.component';

describe('ProfilentrepComponent', () => {
  let component: ProfilentrepComponent;
  let fixture: ComponentFixture<ProfilentrepComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfilentrepComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfilentrepComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
