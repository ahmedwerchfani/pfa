import { Component, OnInit, Input } from '@angular/core';

import { Offre } from '../classes/offre';
import { OffreService } from '../services/offre.service';
import { Postuler } from '../classes/postuler';
import { PostulerService } from '../services/postuler.service';
import { EntrepriseService } from '../services/entreprise.service';
import { Entreprise } from '../classes/entreprise';

@Component({
  selector: 'app-job-details',
  templateUrl: './job-details.component.html',
  styleUrls: ['./job-details.component.css']
})
export class JobDetailsComponent implements OnInit {
ids=localStorage.getItem('idoff')
idofff=parseInt(this.ids);
offre:Offre
offres:Offre[]
postuler:Postuler
postulers:Postuler[]
entreprise:Entreprise
entreprises:Entreprise[]
identrep=localStorage.getItem('identreprise');
affiche=false

submitted = false;
  constructor( private offreservice:OffreService,private postulerservice: PostulerService,private entrepriseservice:EntrepriseService) {}

  ngOnInit() {
   
    this.getOffre(this.idofff);
 
    this.newPostuler();
   if(localStorage.getItem('id')==null){
     this.affiche=true
   }
 }
 getOffre(i:number) {
   this.offreservice.getOffre(i).then(offres => this.offre = offres);
  }
  getEntreprise(i:number) {
    this.entrepriseservice.getEntreprise(i).then(entreprises => this.entreprise = entreprises);
   }
  
  newPostuler(): void {
  this.submitted = false;
  this.postuler = new Postuler();
  this.postuler.emetteur = localStorage.getItem('id');
   const   displayDate = new Date();
   this.postuler.date = displayDate.toLocaleDateString().toString();
  this.postuler.mail=localStorage.getItem('email');   
   this.postuler.recepteur=localStorage.getItem('identreprise');
  }
 
  

  onSubmit() {
      this.submitted = true;
      this.save();}
    
    private save(): void {
      this.postulerservice.create(this.postuler);}
     
}
