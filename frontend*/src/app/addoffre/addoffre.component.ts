import { Component, OnInit } from '@angular/core';
import { Offre } from '../classes/offre';
import { OffreService } from '../services/offre.service';

@Component({
  selector: 'app-addoffre',
  templateUrl: './addoffre.component.html',
  styleUrls: ['./addoffre.component.css']
})
export class AddoffreComponent implements OnInit {

  offre: Offre;
  submitted = false;
  Success=false;
  constructor(private offreservice: OffreService) { }

  ngOnInit() {
    this.submitted = false;
    this.offre = new Offre();
  }
  onSubmit() {
    this.submitted = true;
    this.save();
  }
  private save(): void {
    this.offre.identreprise=parseInt(localStorage.getItem('id'));
    this.offreservice.create(this.offre);
    this.Success=true;

  }
  goBack(): void {
    window.location.replace('accueil');
  }
  deco()
  { 
    localStorage.clear(); 
    window.location.replace('accueil');
  } 

}