import { Component, OnInit, Input } from '@angular/core';
import { Cv } from '../classes/cv';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.css']
})
export class UserDetailsComponent implements OnInit {
    @Input()
    
    cv: Cv;
    constructor() { }
  
    ngOnInit() {
    }
  
  }
  