import { Component, OnInit } from '@angular/core';
import { User } from '../classes/users';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-profiladmin',
  templateUrl: './profiladmin.component.html',
  styleUrls: ['./profiladmin.component.css']
})
export class ProfiladminComponent implements OnInit {

  test: string;
  users: User[];
  
  user: User;
  
    constructor(private userservice: UserService) {
  

     }

  ngOnInit() {
    if (localStorage.length === 0) {
          window.location.replace('auth');
    }
    this.getUsers();
    
  }
  getUsers() {
    this.userservice.getUsers().then(users => this.users = users);
   }
   

   onSelect(us: User): void {
    this.user = us;
    console.log(us.id);
  }
  delete(): void {

    this.userservice.delete(this.user.id).then(() => this.goBack());
  }
  
  goBack(): void {
    window.location.replace('accueil');
  }
}
