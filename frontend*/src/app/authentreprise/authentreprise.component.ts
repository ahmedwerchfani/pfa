import { Component, OnInit } from '@angular/core';
import { Entreprise } from '../classes/entreprise';
import { EntrepriseService } from '../services/entreprise.service';

@Component({
  selector: 'app-authentreprise',
  templateUrl: './authentreprise.component.html',
  styleUrls: ['./authentreprise.component.css']
})
export class AuthentrepriseComponent implements OnInit {
entreprise: Entreprise[];
entr: Entreprise;
submitted = false;
Success=false;
  constructor(private entrepriseservice: EntrepriseService) { }
  ngOnInit() {
    localStorage.clear();
this.entr = new Entreprise();
     this. getEntreprises();

}
getEntreprises() {
 this.entrepriseservice. getEntreprises().then(entreprise => this.entreprise = entreprise );
}
newEntreprise(): void {
this.submitted = false;
this.entr = new Entreprise();
}
onSubmit() {
this.submitted = true;
this.save();
}

private save(): void {

this.getEntreprises();
localStorage.setItem('email', this.entr.email);
if(this.entr.email === "admin" && this.entr.mdp === "admin")
{
  localStorage.setItem('nom', "admin");
window.location.replace('profiladmin');
}else{
for( const s of this.entreprise) {
if ( (this.entr.email === s.email) &&(this.entr.mdp === s.mdp) ) {
   localStorage.setItem('id', String(s.id));
   localStorage.setItem('email',String(s.email));
   localStorage.setItem('type',"entrep");
window.location.replace('profilentreprise');
this.submitted=false;
this.Success= true;
}}}
console.log(localStorage.getItem('email'));
}

}

  

