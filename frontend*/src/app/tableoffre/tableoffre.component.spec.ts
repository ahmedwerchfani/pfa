import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableoffreComponent } from './tableoffre.component';

describe('TableoffreComponent', () => {
  let component: TableoffreComponent;
  let fixture: ComponentFixture<TableoffreComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableoffreComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableoffreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
