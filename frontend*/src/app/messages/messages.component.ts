import { Component, OnInit } from '@angular/core';
import { Message } from '../classes/message';
import { MessageService } from '../services/messages.service';
import { UserService } from '../services/user.service';
import { User } from '../classes/users';

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.css']
})
export class MessagesComponent implements OnInit {
  msgs:Message[];
  msg:Message;
  users:User[];
  user:User;
  is:string;
  a:number;
  recpt=(localStorage.getItem('id'));
  n=(localStorage.getItem('nom'));
    constructor(private msgservice:MessageService, private userservice:UserService) {
  

     }

  ngOnInit() {
    
    
    this.getMsgs(this.recpt);
    this.is=this.msg.emetteur
    this.a=parseInt(this.is);
    this.getUsers(this.a);
    console.log(this.a);
     
  }
 
   getMsgs(i:string){
    this.msgservice.getmess(i).then(msgs => this.msgs = msgs);
   }
   getUsers(i:number) {
    this.userservice.getUser(i).then(users => this.user = users);
    
   }
   onSelect(ms: Message): void {
    this.msg = ms;
    console.log(ms.id);
  }
  delete(): void {
   this.msgservice.delete(this.msg.id).then(() => this.goBack());
  }
  goBack(): void {
    window.location.replace('accueil');


    
  }
  deco()
{ 
  localStorage.clear(); 
  window.location.replace('accueil');
} 
}