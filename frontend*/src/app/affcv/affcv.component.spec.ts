import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AffcvComponent } from './affcv.component';

describe('AffcvComponent', () => {
  let component: AffcvComponent;
  let fixture: ComponentFixture<AffcvComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AffcvComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AffcvComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
