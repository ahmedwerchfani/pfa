import { Offre } from './../classes/offre';
import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
@Injectable({
  providedIn: 'root'
})
export class OffreService {
  private headers = new Headers({'Content-Type': 'application/json'});




  constructor(private http: Http) { }
   getOffres(): Promise<Offre[]> {
    return this.http.get('http://localhost:8080/api/offre')
      .toPromise()
      .then(response => response.json() as Offre[])
      .catch();
  }
  getOffre(id:number): Promise<Offre> {
    return this.http.get(`http://localhost:8080/api/offres/${id}`)
      .toPromise()
      .then(response => response.json() as Offre)
      .catch();
  }
  getOff(id:number): Promise<Offre[]> {
    return this.http.get(`http://localhost:8080/api/offr/${id}`)
      .toPromise()
      .then(response => response.json() as Offre[])
      .catch();
  }
  create(offre: Offre): Promise<Offre> {
    return this.http
      .post('http://localhost:8080/api/offres', JSON.stringify(offre), {headers : this.headers})
      .toPromise()
      .then(res => res.json() as Offre)
      .catch(this.handleError);
  }
  delete(id: number): Promise<void> {
    const url = `http://localhost:8080/api/offres/${id}`;
    return this.http.delete(url, {headers: this.headers})
      .toPromise()
      .then(() => null)
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('Error', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}

