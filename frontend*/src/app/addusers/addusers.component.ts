import { Component, OnInit, NgModule } from '@angular/core';
import { UserService } from './../services/user.service';
import { User } from './../classes/users';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { AppComponent } from '../app.component';

@Component({
  selector: 'app-addusers',
  templateUrl: './addusers.component.html',
  styleUrls: ['./addusers.component.css']
})
@NgModule({
  imports: [
    BrowserModule,
    FormsModule],
  declarations: [
    AppComponent
  ],
  bootstrap: [AppComponent]
})
export class AddusersComponent implements OnInit {
  user: User;
  submitted = false;
  constructor(private userservice: UserService) { }

  ngOnInit() {
    this.submitted = false;
    this.user = new User();
  }
  
  onSubmit() {
    this.submitted = true;
    this.save();
  }
  private save(): void {
    this.userservice.create(this.user);

  }
  goBack(): void {
    window.location.replace('accueil');
  }


}
