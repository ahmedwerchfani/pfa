import { Parcours} from './../classes/parcours';
import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
@Injectable({
  providedIn: 'root'
})
export class ParcoursService {
  private headers = new Headers({'Content-Type': 'application/json'});




  constructor(private http: Http) { }
   getParcous(): Promise<Parcours[]> {
    return this.http.get('http://localhost:8080/api/parcours')
      .toPromise()
      .then(response => response.json() as Parcours[])
      .catch();
  }
  getParcou(iduser:number): Promise<Parcours[]> {
    return this.http.get(`http://localhost:8080/api/parcouss/${iduser}`)
      .toPromise()
      .then(response => response.json() as Parcours[])
      .catch();
  }
  
  create(parcour: Parcours): Promise<Parcours> {
    return this.http
      .post('http://localhost:8080/api/parcourss', JSON.stringify(parcour), {headers : this.headers})
      .toPromise()
      .then(res => res.json() as Parcours)
      .catch(this.handleError);
  }
  delete(id: number): Promise<void> {
    const url = `http://localhost:8080/api/parcourss/${id}`;
    return this.http.delete(url, {headers: this.headers})
      .toPromise()
      .then(() => null)
      .catch(this.handleError);
  }
  createparcours(id:number,parcours:Parcours): Promise<Parcours> {
    return this.http
      .put(`http://localhost:8080/api/parcourss/${id}`,JSON.stringify(parcours), {headers : this.headers})
      .toPromise()
      .then(res => res.json() as Parcours)
      .catch(this.handleError);
  }
  private handleError(error: any): Promise<any> {
    console.error('Error', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}
