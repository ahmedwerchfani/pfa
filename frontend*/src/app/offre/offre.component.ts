import { Component, OnInit } from '@angular/core';
import { Offre } from '../classes/offre';
import { OffreService } from '../services/offre.service';

@Component({
  selector: 'app-offre',
  templateUrl: './offre.component.html',
  styleUrls: ['./offre.component.css']
})
export class OffreComponent implements OnInit {
  offre: Offre;
  offres:Offre[];
  a=(localStorage.getItem('id'));
  id=parseInt(this.a);
  
  constructor(private offreservice: OffreService) { }

  ngOnInit() {
    
    this.getUsers(this.id);
    
    }
  
  getUsers(i:number) {
  this.offreservice.getOff(i).then(offres =>this.offres=offres)
   }
   
  onSelect(of: Offre): void {
    this.offre= of;
    console.log(of.id);
  }
  delete(): void {
  
    this.offreservice.delete(this.offre.id);
    this.offres.splice(1);

  }
  goBack(): void {
    window.location.replace('off'); 
  }
  deco()
{ 
  localStorage.clear(); 
  window.location.replace('accueil');
} 
  
  }