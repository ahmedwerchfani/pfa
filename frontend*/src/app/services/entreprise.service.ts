import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import { Entreprise } from './../classes/entreprise';
@Injectable({
  providedIn: 'root'
})
export class EntrepriseService {
  private headers = new Headers({'Content-Type': 'application/json'});


  constructor(private http: Http) { }
   getEntreprises(): Promise<Entreprise[]> {
    return this.http.get('http://localhost:8080/api/entreprise')
      .toPromise()
      .then(response => response.json() as Entreprise[])
      .catch();
  }
  getEntreprise(id:number): Promise<Entreprise> {
    return this.http.get(`http://localhost:8080/api/entreprises/${id}`)
      .toPromise()
      .then(response => response.json() as Entreprise)
      .catch();
  }
  create(entreprise: Entreprise): Promise<Entreprise> {
    return this.http
      .post('http://localhost:8080/api/entreprises', JSON.stringify(entreprise), {headers : this.headers})
      .toPromise()
      .then(res => res.json() as Entreprise)
      .catch(this.handleError);
  }
  createuser(id:number,user:Entreprise): Promise<Entreprise> {
    return this.http
      .put(`http://localhost:8080/api/entreprise/${id}`,JSON.stringify(user), {headers : this.headers})
      .toPromise()
      .then(res => res.json() as Entreprise)
      .catch(this.handleError);
  }
  delete(id: number): Promise<void> {
    const url = `http://localhost:8080/api/entreprises/${id}`;
    return this.http.delete(url, {headers: this.headers})
      .toPromise()
      .then(() => null)
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('Error', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}