/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.reseau.Controller;

/**
 *
 * @author orange
 */
import Exception.ResourceNotFoundException;
import com.example.reseau.Entities.Entreprise;
import com.example.reseau.Repository.EntrepriseRepository;
import javax.transaction.Transactional;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;


/**
 *
 * @author orange
 */
@RestController //pour application rest resultat json et xml
@Transactional //accepte les transaction des autre application
@RequestMapping("/api")
@CrossOrigin(origins = "localhost:4200", maxAge = 3600)//le port de angulair et maxage pour securite
public class EntrepriseController {

    @Autowired //
    EntrepriseRepository entrepriseRepository;

    // Get All Entreprises
@GetMapping("/entreprise")
public List<Entreprise> getEntreprises() {
    return (List<Entreprise>) entrepriseRepository.findAll();
}
// Create a new Entreprise
@PostMapping("/entreprises")
public Entreprise createEntreprise(@Valid @RequestBody Entreprise entreprise) {
    return entrepriseRepository.save(entreprise);
}

// Get a Single Entreprise
@GetMapping("/entreprises/{id}")
public Entreprise getEntrepriseById(@PathVariable(value = "id") Long entrepriseId) {
    return entrepriseRepository.findById(entrepriseId)
            .orElseThrow(() -> new ResourceNotFoundException("Entreprise", "id", entrepriseId));
}
    // Update a Entreprise
@PutMapping("/entreprise/{id}")
public Entreprise updateEntreprise(@PathVariable(value = "id") Long entrepriseId,
                                        @Valid @RequestBody Entreprise entrepriseDetails) {

    Entreprise entreprise = entrepriseRepository.findById(entrepriseId)
            .orElseThrow(() -> new ResourceNotFoundException("Entreprise", "id", entrepriseId));

    entreprise.setTitre(entrepriseDetails.getTitre());
    entreprise.setDescription(entrepriseDetails.getDescription());
    

    entreprise.setEmail(entrepriseDetails.getEmail());
    entreprise.setAdresse(entrepriseDetails.getAdresse());
    entreprise.setSecteur(entrepriseDetails.getSecteur());
    entreprise.setTel(entrepriseDetails.getTel());
        entreprise.setMdp(entrepriseDetails.getMdp());
    Entreprise updatedEntreprise = entrepriseRepository.save(entreprise);
    return updatedEntreprise;
}

// Delete a Entreprise
@DeleteMapping("/entreprises/{id}")
public ResponseEntity<?> deleteEntreprise(@PathVariable(value = "id") Long entrepriseId) {
    Entreprise entreprise = entrepriseRepository.findById(entrepriseId)
            .orElseThrow(() -> new ResourceNotFoundException("Entreprise", "id", entrepriseId));

    entrepriseRepository.delete(entreprise);

    return ResponseEntity.ok().build();
}
}
