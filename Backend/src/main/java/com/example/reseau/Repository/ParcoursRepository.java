/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.reseau.Repository;

import com.example.reseau.Entities.Parcours;
import java.util.List;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author orange
 */
public interface ParcoursRepository extends CrudRepository<Parcours, Long> {

    public List<Parcours> findByIduser(Long iduser);

   
    
}
