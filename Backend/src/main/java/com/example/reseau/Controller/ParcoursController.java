/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.example.reseau.Controller;

/**
 *
 * @author orange
 */
import Exception.ResourceNotFoundException;
import com.example.reseau.Entities.Parcours;
import com.example.reseau.Repository.ParcoursRepository;
import javax.transaction.Transactional;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;


/**
 *
 * @author orange
 */
@RestController //pour application rest resultat json et xml
@Transactional //accepte les transaction des autre application
@RequestMapping("/api")
@CrossOrigin(origins = "localhost:4200", maxAge= 3600)//le port de angulair et maxage pour securite
public class ParcoursController {

    @Autowired //
    ParcoursRepository parcoursRepository;

    // Get All Parcourss
@GetMapping("/parcours")
public List<Parcours> getParcourss() {
    return (List<Parcours>) parcoursRepository.findAll();
}
// Create a new Parcours
@PostMapping("/parcourss")
public Parcours createParcours(@Valid @RequestBody Parcours parcours) {
    return parcoursRepository.save(parcours);
}

// Get a Single Parcours
@GetMapping("/parcourss/{id}")
public Parcours getParcoursById(@PathVariable(value = "id") Long parcoursId) {
    return parcoursRepository.findById(parcoursId)
            .orElseThrow(() -> new ResourceNotFoundException("Parcours", "id", parcoursId));
}
    // Update a Parcours

@GetMapping("/parcouss/{iduser}")
public List<Parcours> getParcoursByIduser(@PathVariable Long iduser) {
    return (List<Parcours>) parcoursRepository.findByIduser(iduser);

}
// Delete a Parcours
@DeleteMapping("/parcourss/{id}")
public ResponseEntity<?> deleteParcours(@PathVariable(value = "id") Long parcoursId) {
    Parcours parcours = parcoursRepository.findById(parcoursId)
            .orElseThrow(() -> new ResourceNotFoundException("Parcours", "id", parcoursId));

    parcoursRepository.delete(parcours);

    return ResponseEntity.ok().build();
}
@PutMapping("/parcourss/{id}")
public Parcours updateParcours(@PathVariable(value = "id") Long parcoursId,
                                        @Valid @RequestBody Parcours parcoursDetails) {

    Parcours parcours = parcoursRepository.findById(parcoursId)
            .orElseThrow(() -> new ResourceNotFoundException("Parcours", "id", parcoursId));

    parcours.setPoste(parcoursDetails.getPoste());
    parcours.setLieu(parcoursDetails.getLieu());
    

    parcours.setDebut(parcoursDetails.getDebut());
    parcours.setPeriode(parcoursDetails.getPeriode());
    parcours.setTitre(parcoursDetails.getTitre());
    parcours.setDescription(parcoursDetails.getDescription());
    parcours.setEntreprise(parcoursDetails.getEntreprise());
   
    Parcours updatedParcours = parcoursRepository.save(parcours);
    return updatedParcours;
}
}
