import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddparcoursComponent } from './addparcours.component';

describe('AddparcoursComponent', () => {
  let component: AddparcoursComponent;
  let fixture: ComponentFixture<AddparcoursComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddparcoursComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddparcoursComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
