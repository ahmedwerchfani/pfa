import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AccueilComponent } from './accueil/accueil.component';
import { AppComponent } from './app.component';
import { UpComponent } from './up/up.component';
import { FileSelectDirective, FileDropDirective } from 'ng2-file-upload';
import { AuthComponent } from './auth/auth.component';


import { AddusersComponent } from './addusers/addusers.component';
import { MessagesComponent } from './messages/messages.component';
import { SendmessageComponent } from './sendmessage/sendmessage.component';
import { DocumentsComponent } from './documents/documents.component';
import { CreatedocumentsComponent } from './createdocuments/createdocuments.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { OffreComponent } from './offre/offre.component';
import { ProfiladminComponent } from './profiladmin/profiladmin.component';
import { TablentrepriseComponent } from './tablentreprise/tablentreprise.component';
import { TableoffreComponent } from './tableoffre/tableoffre.component';
import { RechercheComponent } from './recherche/recherche.component';
import { FindcandidatComponent } from './findcandidat/findcandidat.component';
import { AddentreprisesComponent } from './addentreprises/addentreprises.component';
import { AddcvsComponent } from './addcvs/addcvs.component';
import { AddparcoursComponent } from './addparcours/addparcours.component';
import { AddoffreComponent } from './addoffre/addoffre.component';
import { AffcvComponent } from './affcv/affcv.component';
import { AuthentrepriseComponent } from './authentreprise/authentreprise.component';
import { JobDetailsComponent } from './job-details/job-details.component';
import { createComponent } from '@angular/compiler/src/core';
import { ProfilentrepComponent } from './profilentrep/profilentrep.component';
import { JobdetailComponent } from './jobdetail/jobdetail.component';
import { CvutilisateurComponent } from './cvutilisateur/cvutilisateur.component';
import { MessagentrepriseComponent } from './messagentreprise/messagentreprise.component';



const routes: Routes = [
  {path: '', redirectTo: 'accueil', pathMatch: 'full'},
  {path: 'accueil', component: AccueilComponent},
  {path: 'message', component: SendmessageComponent},

  {path: 'folder', component: DocumentsComponent},
  {path: 'file', component: UpComponent},
  {path: 'ins', component: AddusersComponent},
  {path: 'auth', component: AuthComponent},
  {path: 'off',component:OffreComponent},
  {path: 'profiladmin',component:ProfiladminComponent},
{path:'tableEntreprise',component:TablentrepriseComponent},
{path:'tableOffre',component:TableoffreComponent},
{path:'rech',component:RechercheComponent},
{path:'cv',component:FindcandidatComponent},
{path:'inse',component:AddentreprisesComponent},
{path:'inscv',component:AddcvsComponent},
{path:'insparco',component:AddparcoursComponent},
{path:'insoffre',component:AddoffreComponent},
{path:'msg',component:MessagesComponent},
{path:'affcv',component:AffcvComponent},
{path:'authentreprise',component:AuthentrepriseComponent},
{path:'jobdetail',component:JobDetailsComponent},
{path:'up',component:UpComponent},
{path:'profilentreprise',component:ProfilentrepComponent},
{path:'cvv',component:JobdetailComponent},
{path:"cvutilisateur",component:CvutilisateurComponent},
{path:"msgentreprise",component:MessagentrepriseComponent}

];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
