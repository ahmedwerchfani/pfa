import { Component, OnInit } from '@angular/core';
import { Cv } from '../classes/cv';
import { CvService } from '../services/cv.service';

@Component({
  selector: 'app-addcvs',
  templateUrl: './addcvs.component.html',
  styleUrls: ['./addcvs.component.css']
})
export class AddcvsComponent implements OnInit {
  a=(localStorage.getItem('id'));
  id=parseInt(this.a);
  cvi:Cv;
  submitted = false;
  constructor(private cviservice: CvService) { }

  ngOnInit() {
    this.submitted = false;

    this.cvi = new Cv();
    this.cvi.id=this.id;
    this.cvi.iduser=this.id;
  }
  onSubmit() {
    this.submitted = true;
    this.save();
  }
  private save(): void {
    
    

  }
  goBack(): void {
    window.location.replace('profile');
  }


}