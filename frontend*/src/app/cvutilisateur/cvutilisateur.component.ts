import { Component, OnInit, Input } from '@angular/core';
import { Parcours } from '../classes/parcours';
import { Scolarite } from '../classes/scolarite';
import { ParcoursService } from '../services/parcours.service';
import { ScolariteService } from '../services/scolarite.service';
import { Cv } from '../classes/cv';
import { CvService } from '../services/cv.service';

import { User } from '../classes/users';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-cvutilisateur',
  templateUrl: './cvutilisateur.component.html',
  styleUrls: ['./cvutilisateur.component.css']
})
export class CvutilisateurComponent implements OnInit {
//utilisateur
users: User[];
  
  user: User;

  //parcours
  pracourss:Parcours[];
  parcour:Parcours;
  
//fin parcour 
//scolarite

scolarites:Scolarite[];
scolarit:Scolarite;
scolarite:Scolarite;
   
//fin scolarite
//cv
 cvs:Cv[];
 cv:Cv;
 
//fin cv
  a=(localStorage.getItem('id'));
  id=parseInt(this.a);
  submitted = false;
  constructor(private userservice:UserService,private parcourservice:ParcoursService,private scolariteservice:ScolariteService,private cvservice:CvService ) { }
 
  ngOnInit() {
   //parcour 
  this.getUsers(this.id);
  this.submitted = false;
 this.getParcour(this.id);
 //scolarite 
 this.getScolarit(this.id);
 this.scolarit= new Scolarite();
 //cv
 this.getCv(this.id);
 this.cv= new Cv();
  }

//utilisateur
getUsers(i:number) {
  this.userservice.getUser(i).then(users => this.user = users);
  
 }

//parcour
getParcour(i:number) {
this.parcourservice.getParcou(i).then(parcourss =>this.pracourss=parcourss);
}
 


//scolarite

getScolarit(i:number) {
  this.scolariteservice.getScola(i).then(scolarites =>this.scolarites=scolarites);
}

onSelectscolarite(scola: Scolarite): void {
  this.scolarite= scola;
  console.log(scola.id);
}




//cv
getCv(i:number) {
  
  this.cvservice.getCv(i).then(cv =>this.cv=cv);
  
   }
   
  

}
