/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.reseau.Controller;

import Exception.ResourceNotFoundException;
import com.example.reseau.Entities.Utilisateur;
import com.example.reseau.Repository.UtilisateurRepository;
import javax.transaction.Transactional;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;


/**
 *
 * @author orange
 */
@RestController //pour application rest resultat json et xml
@Transactional //accepte les transaction des autre application
@RequestMapping("/api")
@CrossOrigin(origins = "localhost:4200", maxAge = 3600)//le port de angulair et maxage pour securite
public class UtilisateurController {

    @Autowired //
    UtilisateurRepository utilisateurRepository;

    // Get All Utilisateurs
@GetMapping("/utilisateur")
public List<Utilisateur> getUtilisateurs() {
    return (List<Utilisateur>) utilisateurRepository.findAll();
}
// Create a new Utilisateur
@PostMapping("/utilisateur")
public Utilisateur createUtilisateur(@Valid @RequestBody Utilisateur utilisateur) {
    return utilisateurRepository.save(utilisateur);
}

// Get a Single Utilisateur
@GetMapping("/utilisateurs/{id}")
public Utilisateur getUtilisateurById(@PathVariable(value = "id") Long utilisateurId) {
    return utilisateurRepository.findById(utilisateurId)
            .orElseThrow(() -> new ResourceNotFoundException("Utilisateur", "id", utilisateurId));
}
    // Update a Utilisateur
@PutMapping("/utilisateur/{id}")
public Utilisateur updateUtilisateur(@PathVariable(value = "id") Long utilisateurId,
                                        @Valid @RequestBody Utilisateur utilisateurDetails) {

    Utilisateur utilisateur = utilisateurRepository.findById(utilisateurId)
            .orElseThrow(() -> new ResourceNotFoundException("Utilisateur", "id", utilisateurId));

    utilisateur.setNom(utilisateurDetails.getNom());
    utilisateur.setPrenom(utilisateurDetails.getPrenom());
    

    utilisateur.setEmail(utilisateurDetails.getEmail());
    utilisateur.setAdresse(utilisateurDetails.getAdresse());
    utilisateur.setProffesion(utilisateurDetails.getProffesion());
    utilisateur.setTel(utilisateurDetails.getTel());
    utilisateur.setAge(utilisateurDetails.getAge());
    utilisateur.setPropos(utilisateurDetails.getPropos());
        utilisateur.setMdp(utilisateurDetails.getMdp());
    Utilisateur updatedUtilisateur = utilisateurRepository.save(utilisateur);
    return updatedUtilisateur;
}

// Delete a Utilisateur
@DeleteMapping("/utilisateurs/{id}")
public ResponseEntity<?> deleteUtilisateur(@PathVariable(value = "id") Long utilisateurId) {
    Utilisateur utilisateur = utilisateurRepository.findById(utilisateurId)
            .orElseThrow(() -> new ResourceNotFoundException("Utilisateur", "id", utilisateurId));

    utilisateurRepository.delete(utilisateur);

    return ResponseEntity.ok().build();
}
}