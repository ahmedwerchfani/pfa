import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MessagentrepriseComponent } from './messagentreprise.component';

describe('MessagentrepriseComponent', () => {
  let component: MessagentrepriseComponent;
  let fixture: ComponentFixture<MessagentrepriseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MessagentrepriseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MessagentrepriseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
