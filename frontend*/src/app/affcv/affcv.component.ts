import { Component, OnInit } from '@angular/core';
import { Cv } from '../classes/cv';
import { CvService } from '../services/cv.service';
import { Parcours } from '../classes/parcours';
import { ParcoursService } from '../services/parcours.service';

@Component({
  selector: 'app-affcv',
  templateUrl: './affcv.component.html',
  styleUrls: ['./affcv.component.css']
})
export class AffcvComponent implements OnInit {

  cvs: Cv[];
  cv: Cv;
  pracourss:Parcours[];
  parcour:Parcours;
  a=(localStorage.getItem('id'));
  id=parseInt(this.a);
  submitted = false;
  constructor( private cvservice:CvService, private parcourservice:ParcoursService) { }

  
  ngOnInit() {
    
  this.getUsers(this.id);
  this.submitted = false;
  }

getUsers(i:number) {
this.cvservice.getCv(i).then(cvs => this.cv = cvs);
this.parcourservice.getParcou(i).then(parcourss =>this.pracourss=parcourss);

 }
 
onSelect(par: Parcours): void {
  this.parcour= par;
  console.log(par.id);
}
delete(): void {

  this.parcourservice.delete(this.parcour.id).then(() => this.goBack());
}
goBack(): void {
  window.location.replace('accueil');
}
onSubmit() {
  this.submitted = true;
  this.save();
}
private save(): void {
 
  this.cvservice.createcv(this.cv.id,this.cv);

}
}
