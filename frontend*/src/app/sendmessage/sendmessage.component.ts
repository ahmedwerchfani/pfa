import { Message } from './../classes/message';
import { UserService } from './../services/user.service';
import { User } from './../classes/users';
import { MessageService } from './../services/messages.service';
import { Component, OnInit } from '@angular/core';
import { ThrowStmt } from '@angular/compiler';


@Component({
  selector: 'app-sendmessage',
  templateUrl: './sendmessage.component.html',
  styleUrls: ['./sendmessage.component.css']
})
export class SendmessageComponent implements OnInit {
  
  constructor(private userservice: UserService, private messageservice: MessageService) {


   }
users: User[];

message: Message;
messages: Message[];
submitted = false;
is:number;

ngOnInit() {
  if (localStorage.length === 0) {
        window.location.replace('auth');
        this.message = new Message();
        this.message.emetteur = localStorage.getItem('email');
        

  }
  console.log(localStorage.getItem('id'));
  this.getUsers(this.is);
  this.newMessage();
this.is=parseInt(localStorage.getItem('id'))
}
newMessage(): void {
  this.submitted = false;
  this.message = new Message();
  this.message.emetteur = localStorage.getItem('email');
  const   displayDate = new Date();
  console.log(displayDate.toLocaleDateString().toString());
  this.message.date = displayDate.toLocaleDateString().toString();

}

deco()
{ 
  localStorage.clear(); 
  window.location.replace('accueil');
} 

getUsers(i:number) {
  this.userservice.getUsers().then(users => this.users = users);
 
 }

onSubmit() {
  this.submitted = true;
  this.save();
  console.log(this.message);


}
private save(): void {
  this.messageservice.create(this.message);

}
goBack(): void {
  window.location.replace('message');
}

  }


