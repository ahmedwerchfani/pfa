
import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import { Postuler } from '../classes/postuler';
@Injectable({
  providedIn: 'root'
})
export class PostulerService {
  private headers = new Headers({'Content-Type': 'application/json'});




  constructor(private http: Http) { }
   getPostulers(): Promise<Postuler[]> {
    return this.http.get('http://localhost:8080/api/postuler')
      .toPromise()
      .then(response => response.json() as Postuler[])
      .catch();
  }
  
  create( postuler: Postuler): Promise<Postuler> {
    return this.http
      .post('http://localhost:8080/api/postulers', JSON.stringify(postuler), {headers : this.headers})
      .toPromise()
      .then(res => res.json() as Postuler)
      .catch(this.handleError);
  }
  getmess(recepteur:string): Promise<Postuler[]> {
    return this.http.get(`http://localhost:8080/api/postul/${recepteur}`)
      .toPromise()
      .then(response => response.json() as Postuler[])
      .catch();
  }
  delete(id: number): Promise<void> {
    const url = `http://localhost:8080/api/postulers/${id}`;
    return this.http.delete(url, {headers: this.headers})
      .toPromise()
      .then(() => null)
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
 
    return Promise.reject(error.postuler || error);
  }


}