
import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import { Scolarite } from '../classes/scolarite';
@Injectable({
  providedIn: 'root'
})
export class ScolariteService {
  private headers = new Headers({'Content-Type': 'application/json'});

  constructor(private http: Http) { }
   getScolarites(): Promise<Scolarite[]> {
    return this.http.get('http://localhost:8080/api/scolarite')
      .toPromise()
      .then(response => response.json() as Scolarite[])
      .catch();
  }
  getScola(iduser:number): Promise<Scolarite[]> {
    return this.http.get(`http://localhost:8080/api/Scolaritess/${iduser}`)
      .toPromise()
      .then(response => response.json() as Scolarite[])
      .catch();
  }
  
  create(scolarite: Scolarite): Promise<Scolarite> {
    return this.http
      .post('http://localhost:8080/api/scolarites', JSON.stringify(scolarite), {headers : this.headers})
      .toPromise()
      .then(res => res.json() as Scolarite)
      .catch(this.handleError);
  }
  createscolarite(id:number,scolarite:Scolarite): Promise<Scolarite> {
    return this.http
      .put(`http://localhost:8080/api/scolarite/${id}`,JSON.stringify(scolarite), {headers : this.headers})
      .toPromise()
      .then(res => res.json() as Scolarite)
      .catch(this.handleError);
  }
  delete(id: number): Promise<void> {
    const url = `http://localhost:8080/api/scolarites/${id}`;
    return this.http.delete(url, {headers: this.headers})
      .toPromise()
      .then(() => null)
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('Error', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}

