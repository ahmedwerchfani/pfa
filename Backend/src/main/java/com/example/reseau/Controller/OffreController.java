/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.reseau.Controller;

/**
 *
 * @author orange
 */
import Exception.ResourceNotFoundException;
import com.example.reseau.Entities.Offre;
import com.example.reseau.Repository.OffreRepository;
import javax.transaction.Transactional;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;


/**
 *
 * @author orange
 */
@RestController //pour application rest resultat json et xml
@Transactional //accepte les transaction des autre application
@RequestMapping("/api")
@CrossOrigin(origins = "localhost:4200", maxAge = 3600)//le port de angulair et maxage pour securite
public class OffreController {

    @Autowired //
    OffreRepository offreRepository;

    // Get All Offres
@GetMapping("/offre")
public List<Offre> getOffres() {
    return (List<Offre>) offreRepository.findAll();
}
// Create a new Offre
@PostMapping("/offres")
public Offre createOffre(@Valid @RequestBody Offre offre) {
    return offreRepository.save(offre);
}
@GetMapping("/offr/{identreprise}")
public List<Offre> getOffreByIdentreprise(@PathVariable Long identreprise) {
    return (List<Offre>) offreRepository.findByIdentreprise(identreprise);

}
// Get a Single Offre
@GetMapping("/offres/{id}")
public Offre getOffreById(@PathVariable(value = "id") Long offreId) {
    return offreRepository.findById(offreId)
            .orElseThrow(() -> new ResourceNotFoundException("Offre", "id", offreId));
}
    // Update a Offre


// Delete a Offre
@DeleteMapping("/offres/{id}")
public ResponseEntity<?> deleteOffre(@PathVariable(value = "id") Long offreId) {
    Offre offre = offreRepository.findById(offreId)
            .orElseThrow(() -> new ResourceNotFoundException("Offre", "id", offreId));

    offreRepository.delete(offre);

    return ResponseEntity.ok().build();
}
}
