import { Component, OnInit, Input } from '@angular/core';
import { Cv } from '../classes/cv';
import { CvService } from '../services/cv.service';

@Component({
  selector: 'app-findcandidat',
  templateUrl: './findcandidat.component.html',
  styleUrls: ['./findcandidat.component.css']
})
export class FindcandidatComponent implements OnInit {
  cvs: Cv[];
  cv: Cv;
  loading = true;
    constructor(private cvservice: CvService) {
     }

  ngOnInit() {
    this.getUsers();
  }
  getUsers(){
    this.cvservice.getCvs().then(cvs => this.cvs=cvs);
    this.loading = false;
   }
   onSelect(c: Cv): void {
    this.cv = c;
    console.log(c.id);
  }
  
  
  goBack(): void {
    window.location.replace('profilentreprise');}

    onSearchChange(value: string) {
      this.loading = true;
      if (value === '') {
        this.getUsers();
        return;
      }
      this.cvs= this.search(this.cvs, value);
    }
  
    search(cvs: Cv[], searchText: string): Cv[] {
      if (!cvs) {
        return [];
      }
      if (!searchText) { 
        return cvs;
      }
      searchText = searchText.toLowerCase();
      return cvs.filter(it => {
        return it.domaine.toLowerCase().includes(searchText) || it.diplome.toLowerCase().includes(searchText);
      });
    }
    aff(): void {
      
      localStorage.setItem('idutilisaeur',String(this.cv.iduser));
     
      window.location.replace('cvv');
    
    }
 
   
      deco()
      { 
        localStorage.clear(); 
        window.location.replace('accueil');
      } 
    
}
