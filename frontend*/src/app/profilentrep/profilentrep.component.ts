import { Component, OnInit } from '@angular/core';
import { Entreprise } from '../classes/entreprise';
import { EntrepriseService } from '../services/entreprise.service';
@Component({
  selector: 'app-profilentrep',
  templateUrl: './profilentrep.component.html',
  styleUrls: ['./profilentrep.component.css']
})
export class ProfilentrepComponent implements OnInit {
  entreprises: Entreprise[];
  
  entreprise: Entreprise;
  
  a=(localStorage.getItem('id'));
  
  id=parseInt(this.a);
  submitted = false;
  constructor( private entrepriseservice:EntrepriseService) 
  {
    
   }
   

  ngOnInit() {
    
  this.getEntreprises(this.id);
  this.submitted = false;
  //localStorage.setItem('nom',this.entreprise.nom);

  }
  
  
getEntreprises(i:number) {
  this.entrepriseservice.getEntreprise(i).then(entreprises => this.entreprise = entreprises);
  
 }
 

 onSelect(us: Entreprise): void {
  this.entreprise = us;
  console.log(us.id);
}

goBack(): void {
  window.location.replace('accueil');
}
deco()
{ 
  localStorage.clear(); 
  window.location.replace('accueil');
} 
onSubmit() {
  this.submitted = true;
  this.save();
}
private save(): void {
 
  this.entrepriseservice.createuser(this.entreprise.id,this.entreprise);

}
}

 
