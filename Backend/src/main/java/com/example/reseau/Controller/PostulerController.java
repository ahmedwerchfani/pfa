/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.reseau.Controller;

import Exception.ResourceNotFoundException;
import com.example.reseau.Entities.Postuler;
import com.example.reseau.Repository.PostulerRepository;
import javax.transaction.Transactional;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;


/**
 *
 * @author orange
 */
@RestController //pour application rest resultat json et xml
@Transactional //accepte les transaction des autre application
@RequestMapping("/api")
@CrossOrigin(origins = "localhost:4200", maxAge = 3600)//le port de angulair et maxage pour securite
public class PostulerController {

    @Autowired //
    PostulerRepository postulerRepository;

    // Get All Postulers
@GetMapping("/postuler")
public List<Postuler> getPostulers() {
    return (List<Postuler>) postulerRepository.findAll();
}
// Create a new Postuler
@PostMapping("/postulers")
public Postuler createPostuler(@Valid @RequestBody Postuler postuler) {
    return postulerRepository.save(postuler);
}

// Get a Single Postuler
@GetMapping("/postulers/{id}")
public Postuler getPostulerById(@PathVariable(value = "id") Long postulerId) {
    return postulerRepository.findById(postulerId)
            .orElseThrow(() -> new ResourceNotFoundException("Postuler", "id", postulerId));
}
    // Update a Postuler
@GetMapping("/postul/{recepteur}")
public List<Postuler> getPostulerByRecepteur(@PathVariable String recepteur) {
    return postulerRepository.findByRecepteur(recepteur);

}

// Delete a Postuler
@DeleteMapping("/postulers/{id}")
public ResponseEntity<?> deletePostuler(@PathVariable(value = "id") Long postulerId) {
    Postuler postuler = postulerRepository.findById(postulerId)
            .orElseThrow(() -> new ResourceNotFoundException("Postuler", "id", postulerId));

    postulerRepository.delete(postuler);

    return ResponseEntity.ok().build();
}
}