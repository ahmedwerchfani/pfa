import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TablentrepriseComponent } from './tablentreprise.component';

describe('TablentrepriseComponent', () => {
  let component: TablentrepriseComponent;
  let fixture: ComponentFixture<TablentrepriseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TablentrepriseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TablentrepriseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
