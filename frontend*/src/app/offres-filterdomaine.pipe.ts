import { Pipe, PipeTransform } from '@angular/core';
import { Offre } from './classes/offre';

 
@Pipe({
  name: 'offresFilterdomaine'
})
export class OffresFilterdomainePipe implements PipeTransform {

  transform(values) {
    if (values) {
      return values.reverse();
    }
  }




}
