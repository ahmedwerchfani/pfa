import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CvutilisateurComponent } from './cvutilisateur.component';

describe('CvutilisateurComponent', () => {
  let component: CvutilisateurComponent;
  let fixture: ComponentFixture<CvutilisateurComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CvutilisateurComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CvutilisateurComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
