
import { Component, OnInit } from '@angular/core';
import { Parcours } from '../classes/parcours';
import { ParcoursService } from '../services/parcours.service';
import { Scolarite } from '../classes/scolarite';
import { ScolariteService } from '../services/scolarite.service';
import { Cv } from '../classes/cv';
import { CvService } from '../services/cv.service';

@Component({
  selector: 'app-addparcours',
  templateUrl: './addparcours.component.html',
  styleUrls: ['./addparcours.component.css']
})
export class AddparcoursComponent implements OnInit {
//parcours
  pracourss:Parcours[];
  parcour:Parcours;
  parcoure:Parcours;
//fin parcour
//scolarite
   scolarites:Scolarite[];
   scolarit:Scolarite;
   scolarite:Scolarite;
//fin scolarite
//cv
 cvs:Cv[];
 cv:Cv;
 cvt:Cv;
//fin cv
  a=(localStorage.getItem('id'));
  id=parseInt(this.a);
  submitted = false;
  constructor(private parcourservice:ParcoursService,private scolariteservice:ScolariteService,private cvservice:CvService ) { }
 
  ngOnInit() {
   //parcour 
  this.getUsers(this.id);
  this.submitted = false;
  this.parcour= new Parcours();
  this.parcour.iduser=this.id;
 //scolarite
 this.getScolarit(this.id);
 this.scolarit= new Scolarite();
 this.scolarit.iduser=this.id
 //cv
this.getCv(this.id);
this.cv= new Cv();
    this.cv.id=this.id;
    this.cv.iduser=this.id;
  }




//parcour
getUsers(i:number) {
this.parcourservice.getParcou(i).then(parcourss =>this.pracourss=parcourss);
}
 
addFieldValue() {
  this.pracourss.push(this.parcour= new Parcours())
}

deleteFieldValue(index) {
  this.pracourss.splice(index,1);
  this.parcourservice.delete(this.parcoure.id);
}
private save(): void {
  this.parcour.iduser=this.id;
  this.parcourservice.create(this.parcour);

  this.pracourss.push(this.parcour);
 
  this.parcour= new Parcours();
}
onSubmit() {
  this.submitted = true;
  this.save();
}
onSelect(par: Parcours): void {
  this.parcoure= par;
  console.log(par.id);
}
 upd(): void {
 
  this.parcourservice.createparcours(this.parcoure.id,this.parcoure);

}



//scolarite

getScolarit(i:number) {
  this.scolariteservice.getScola(i).then(scolarites =>this.scolarites=scolarites);
}

addFieldValuescolarite() {
  this.scolarites.push(this.scolarit= new Scolarite())
}

deleteFieldValuescolarite(index) {
  this.scolarites.splice(index,1);
  this.scolariteservice.delete(this.scolarite.id);
}
private savescolarite(): void {
  this.scolarit.iduser=this.id;
  this.scolariteservice.create(this.scolarit);

  this.scolarites.push(this.scolarit);
 
  this.scolarit= new Scolarite();
}
onSubmitscolarite() {
  this.submitted = true;
  this.savescolarite();
}
onSelectscolarite(scola: Scolarite): void {
  this.scolarite= scola;
  console.log(scola.id);
}
 updscolarite(): void {
 
  this.scolariteservice.createscolarite(this.scolarite.id,this.scolarite);

}




//cv
getCv(i:number) {
  
  this.cvservice.getCv(i).then(cvs =>this.cv=cvs);
  
   }
   
  private savecv(): void {
    
    this.cvservice.create(this.cv);
    

  }
  onSubmitcv() {
    this.submitted = true;
    this.savecv();
  }
  onSelectcv(scola: Cv): void {
    this.cv= scola;
    console.log(scola.id);
  }
   updcv(): void {
   
    this.cvservice.createcv(this.cv.id,this.cv);
  
  }
  deco()
  { 
    localStorage.clear(); 
    window.location.replace('accueil');
  } 

}
